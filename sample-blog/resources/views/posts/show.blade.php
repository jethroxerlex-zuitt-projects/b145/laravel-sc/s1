@extends('layouts.app')

@section('content')
	<h1>{{$post->title}}</h1>
	<small>Written on {{$post->created_at}}</small>

	<div class="mt-5">
		{{$post->body}}
	</div>
	@if(!Auth::guest())

	<button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#exampleModal">
  Post Comment
</button>
@endif		
@if(count ($post->comments) > 0)
	<h3>Comments:</h3>
	<div class="card">
		<ul class="list-group">
			@foreach($post->comments as $comment)
			<li class="list-group">
				<p class="text-center">{{$comment->content}}</p>
				<p class="text-center">{{$comment->user->name}}</p>
				<p class="text-center">{{$comment->user->name}}</p>
			</li>
			@endforeach
		</ul>
	</div>
@endif
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Comments</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form action="/posts/{{$post->id}}/comment" method="POST">
      		@csrf
      		<div class="form-group">
      		<label for="content">Content:</label>		
      		<input type="text" id="content-input" name="content" class="form-control">
      		</div>
      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        	<button type="submit" class="btn btn-primary">Post Comment</button>
      	</form>
      </div>
     
      </div>
    </div>
  </div>
</div>
@endsection	